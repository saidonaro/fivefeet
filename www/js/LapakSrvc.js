bisnisservices.factory('Lapak_service', function() {
  // Might use a resource here that returns a JSON array
  var Detail_lapak = { nama_lapak: "Lapak A", jumlah_dagangan: 1}

  // Some fake testing data
  var list_dagangan = [
    { title: 'Jus Mangga', id: 1, stock:25, modal: 8000, jual : 15000, qty: 0 },
    { title: 'Jus Melon', id: 2, stock:15, modal: 8000, jual : 15000, qty: 0  },
    { title: 'Jus Semangka', id: 3, stock:5, modal: 8000, jual : 15000, qty: 0  },
    { title: 'Jus Jambu', id: 4, stock:6, modal: 8000, jual : 15000, qty: 0  },
    { title: 'Jus Sirsak', id: 5, stock:4, modal: 8000, jual : 15000, qty: 0  },
    { title: 'Jus Wortel', id: 6, stock:3, modal: 8000, jual : 15000, qty: 0  }
  ];

  return {
    list_dagangan: function() {
      return list_dagangan;
    },
    get: function(daganganID) {
      // Simple index lookup
      return list_dagangan[daganganID];
    },
    detail: function(){
      return Detail_lapak;
    }
  }
});