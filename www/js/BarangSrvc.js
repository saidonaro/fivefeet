bisnisservices.factory('Barang', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var dagangan = [
    { title: 'Reggssssae', id: 1 },
    { title: 'Chilli', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];

  return {
    all: function() {
      return dagangan;
    },
    get: function(friendId) {
      // Simple index lookup
      return dagangan[friendId];
    }
  }
});