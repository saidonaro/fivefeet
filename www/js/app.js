// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','angular.filter', 'bisnis.controllers', 'bisnis.services', 'login.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: 'AppCtrl'
    })
    .state('app.login', {
      url: "/login",
      views: {
        'menuContent' :{
          templateUrl: "templates/login.html",
          controller: 'LoginCtrl'          
        }
      }
    })
    .state('app.no_telepon', {
      url: "/no_telepon",
      views: {
        'menuContent' :{
          templateUrl: "templates/telepon.html",
          controller: 'TeleponCtrl'          
        }
      }
    })
    .state('app.register_usaha', {
      url: "/register_usaha",
      views: {
        'menuContent' :{
          templateUrl: "templates/register_usaha.html",
          controller: 'RegisterUsahaCtrl'          
        }
      }
    })
    .state('app.usaha', {
      url: "/usaha",
      views: {
        'menuContent' :{
          templateUrl: "templates/usaha.html",
          controller: 'UsahaCtrl'          
        }
      }
    })
    .state('app.lapak', {
      url: "/lapak/:lapakID",
      views: {
        'menuContent' :{
          templateUrl: "templates/lapak.html",
          controller: 'LapakCtrl'          
        }
      }
    })
    .state('app.jual', {
      url: "/jual/:sessionID",
      views: {
        'menuContent' :{
          templateUrl: "templates/jual.html",
          controller: 'JualCtrl'          
        }
      }
    })
    .state('app.dagangan', {
      url: "/dagangan/:lapakID",
      views: {
        'menuContent' :{
          templateUrl: "templates/dagangan.html",
          controller: 'DaganganCtrl'
        }
      }
    })
    .state('app.barang', {
      url: "/barang/:barangId",
      views: {
        'menuContent' :{
          templateUrl: "templates/barang.html",
          controller: 'BarangCtrl'
        }
      }
    });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/usaha');
});

