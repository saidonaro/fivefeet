bisnismodule.controller('LapakCtrl', function($scope, $state, $stateParams, Lapak_service) {
	$scope.list_dagangan = Lapak_service.list_dagangan();
	$scope.detail_lapak = Lapak_service.detail();
	
	$scope.keranjang = [];

	$scope.add_barang = function(barang){
		$scope.keranjang.push(barang);
		console.log(barang.title);
	};
	$scope.cek_keranjang= function(){
		localStorage.setItem("keranjang", JSON.stringify($scope.keranjang));		
		$state.go('app.jual', 1);
	};
});